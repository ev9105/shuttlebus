package ev9105.shuttlebus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.sql.Time;

import ev9105.shuttlebus.R;


public class TimeTable extends Fragment {

    private RadioGroup radioGroup;
    private ListView fragment_time_table_lv;
    private TextView actionbar_return_tv;

    private String [] bus_schedule;

    public TimeTable() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_time_table, container, false);
        fragment_time_table_lv = (ListView) v.findViewById(R.id.fragment_time_table_lv);
        radioGroup = (RadioGroup) getActivity().findViewById(R.id.main_radio);
        actionbar_return_tv = (TextView) getActivity().findViewById(R.id.actionbar_return_tv);

        // transfer schedule to string format; default setting SGW
        bus_schedule = new String[Home.sgw_schedule.size()];
        for(int index = 0; index < Home.sgw_schedule.size(); index++){
            bus_schedule[index] = Home.sgw_schedule.get(index);
        }

        fragment_time_table_lv.setAdapter(new TimeTableArrayAdapter(getActivity(), bus_schedule));

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup rGroup, int checkedId)
            {
                switch (checkedId) {
                    case R.id.sgwCampus_rb:

                        bus_schedule = new String[Home.sgw_schedule.size()];
                        for(int index = 0; index < Home.sgw_schedule.size(); index++){
                            bus_schedule[index] = Home.sgw_schedule.get(index);
                        }
                        break;
                    case R.id.loyolaCampus_rb:

                        bus_schedule = new String[Home.loyola_schedule.size()];
                        for(int index = 0; index < Home.loyola_schedule.size(); index++){
                            bus_schedule[index] = Home.loyola_schedule.get(index);
                        }

                        break;
                }

                fragment_time_table_lv.setAdapter(new TimeTableArrayAdapter(getActivity(), bus_schedule));
            }
        });

        actionbar_return_tv.setVisibility(View.VISIBLE);
        actionbar_return_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionbar_return_tv.setVisibility(View.INVISIBLE);
                getActivity().getFragmentManager().beginTransaction().replace(R.id.main_content, new Home()).commit();
            }
        });

        return v;
    }

    // Adapter for creating listview with bus schedule
    public class TimeTableArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        public TimeTableArrayAdapter(Context context, String[] values){
            super(context, R.layout.time_table_row, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.time_table_row, parent, false);
            TextView time_table_row_tv = (TextView) rowView.findViewById(R.id.time_table_row_tv);
            time_table_row_tv.setText(values[position]);

            return rowView;
        }
    }

}
