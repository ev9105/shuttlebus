package ev9105.shuttlebus.fragment;

import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import ev9105.shuttlebus.R;

public class Home extends Fragment {

    private static Typeface OpenSans_Regular;

    private RadioGroup radioGroup;
    private TextView fragment_home_time_tv;
    private TextView fragment_home_curCampus_tv;
    private TextView fragment_home_timeTitle_tv;

    public static ArrayList<String> loyola_schedule = new ArrayList<String>();
    public static ArrayList<String> sgw_schedule = new ArrayList<String>();

    /*public static int [] from_loyola = {745, 815, 845, 915, 945, 1015, 1100, 1130, 1200, 1230, 1300,
            1330, 1400, 1430, 1500, 1530, 1600, 1630, 1715, 1745, 1815,
            1845, 1915, 1930, 1945, 2025, 2145, 2235};
    public static int [] from_sgw = {745, 815, 845, 915, 945, 1010, 1040, 1130, 1200, 1230, 1300, 1330,
            1400, 1430, 1500, 1530, 1600, 1625, 1655, 1745, 1815, 1845, 1915,
            1945, 2000, 2050, 2210, 2300};*/
    //public static int [] from_loyola = null;
    //public static int [] from_sgw = null;

    public static ArrayList<Integer> from_loyola_schedule = new ArrayList<Integer>();
    public static ArrayList<Integer> from_sgw_schedule = new ArrayList<Integer>();
    public static String from_loyola_unknown_start;
    public static String from_loyola_unknown_end;
    public static String from_sgw_unknown_start;
    public static String from_sgw_unknown_end;

    public Home() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        radioGroup = (RadioGroup) getActivity().findViewById(R.id.main_radio);
        fragment_home_time_tv = (TextView) v.findViewById(R.id.fragment_home_time_tv);
        fragment_home_curCampus_tv = (TextView) v.findViewById(R.id.fragment_home_curCampus_tv);
        fragment_home_timeTitle_tv = (TextView) v.findViewById(R.id.fragment_home_timeTitle_tv);

        // fonts
        this.OpenSans_Regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        fragment_home_time_tv.setTypeface(OpenSans_Regular);
        fragment_home_curCampus_tv.setTypeface(OpenSans_Regular);
        fragment_home_timeTitle_tv.setTypeface(OpenSans_Regular);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup rGroup, int checkedId)
            {
                try {
                    //Get current time
                    Calendar c = Calendar.getInstance();
                    int cur_hour = c.get(Calendar.HOUR_OF_DAY);
                    int cur_minute = c.get(Calendar.MINUTE);
                    int cur_day = c.get(Calendar.DAY_OF_WEEK);
                    int cur_time = cur_hour*100 + cur_minute;

                    //time difference
                    String difference_time = "Not Available";
                    fragment_home_timeTitle_tv.setVisibility(View.VISIBLE);

                    if(cur_day == Calendar.SUNDAY || cur_day == Calendar.SATURDAY){
                        difference_time = "No available bus";
                        fragment_home_time_tv.setText(difference_time);
                    }else{
                        // date format
                        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                        Date cur_date = format.parse(cur_hour+":"+cur_minute);

                        switch (checkedId) {
                            case R.id.sgwCampus_rb:

                                //traverse the S.G.W time schedule
                                for (int i = 0; i < from_sgw_schedule.size(); i++) {
                                    if (cur_time < from_sgw_schedule.get(i)) {

                                        // calculate the time difference
                                        Date next_bus_date = format.parse(from_sgw_schedule.get(i)/100 + ":" + from_sgw_schedule.get(i)%100);
                                        long difference = next_bus_date.getTime() - cur_date.getTime();
                                        difference_time = difference/(60*1000) + " min";
                                        break;
                                    }
                                }
                                fragment_home_curCampus_tv.setText("S.G.W");
                                fragment_home_time_tv.setText(difference_time);
                                break;
                            case R.id.loyolaCampus_rb:

                                //traverse the Loyola time schedule
                                for (int i = 0; i < from_loyola_schedule.size(); i++) {
                                    if (cur_time < from_loyola_schedule.get(i)) {

                                        // calculate the time difference
                                        Date next_bus_date = format.parse(from_loyola_schedule.get(i)/100 + ":" + from_loyola_schedule.get(i)%100);
                                        long difference = next_bus_date.getTime() - cur_date.getTime();
                                        difference_time = difference/(60*1000) + " min";
                                        break;
                                    }
                                }
                                fragment_home_curCampus_tv.setText("Loyola");
                                fragment_home_time_tv.setText(difference_time);
                                break;
                        }
                    }

                } catch (ParseException e) {
                    // when parse time have exception, need set time error here and send to analytics
                }
            }
        });

        // default show time....
        fragment_home_timeTitle_tv.setVisibility(View.INVISIBLE);

        // tv callback to TimeTable fragment
        fragment_home_curCampus_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Fragment fragment = new TimeTable();
                transaction.replace(R.id.main_content, fragment);
                transaction.commit();
            }
        });

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        /****************************************************
        * Every 20 mins possible explanation or any other word
        * Need handle this special situation
        *****************************************************/

        for(int i = 0; i < loyola_schedule.size(); i++){
            // "Every 20-25 mins" and Empty String Exception
            if(!loyola_schedule.get(i).contains(":")){
                if(!loyola_schedule.get(i).equals("null")){
                    from_loyola_unknown_start = loyola_schedule.get(i - 1);
                    from_loyola_unknown_end = loyola_schedule.get(i + 1);
                }
                continue;
            }

            String[] temp = loyola_schedule.get(i).split(":");
            int temp_time = Integer.parseInt(temp[0])*100 + Integer.parseInt(temp[1]);
            from_loyola_schedule.add(temp_time);
        }
        for(int i = 0; i < sgw_schedule.size(); i++){
            // "Every 20-25 mins" and Empty String Exception
            if(!sgw_schedule.get(i).contains(":")){
                if(!sgw_schedule.get(i).equals("null")){
                    from_sgw_unknown_start = sgw_schedule.get(i - 1);
                    from_sgw_unknown_end = sgw_schedule.get(i + 1);
                }
                continue;
            }

            String[] temp = sgw_schedule.get(i).split(":");
            int temp_time = Integer.parseInt(temp[0])*100 + Integer.parseInt(temp[1]);
            from_sgw_schedule.add(temp_time);
        }

        Collections.sort(from_loyola_schedule);
        Collections.sort(from_sgw_schedule);

        return v;
    }

}
