package ev9105.shuttlebus.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import ev9105.shuttlebus.R;
import ev9105.shuttlebus.fragment.Home;

public class SplashScreen extends Activity {
    long start;
    long end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.SplashScreen);
        setContentView(R.layout.activity_splash_screen);

        start = System.currentTimeMillis();
        loadParseData();
    }

    private void loadParseData(){
        //*******************************************************************
        //**********************  Setting Connection   **********************
        //*******************************************************************

        Parse.initialize(SplashScreen.this, getResources().getString(R.string.PARSE_APPLICATION_ID),
                getResources().getString(R.string.PARSE_CLIENT_KEY));
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        // If you would like all objects to be private by default, remove this line.
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

        //get loyola schedule
        int cur_day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        if(cur_day == Calendar.SATURDAY || cur_day == Calendar.SUNDAY){

        }else{
            if(cur_day == Calendar.FRIDAY){
                // friday schedule
                ParseQuery<ParseObject> schedule_query = ParseQuery.getQuery("friday");
                schedule_query.selectKeys(Arrays.asList("Loyola", "SGW"));
                schedule_query.orderByAscending("order");

                schedule_query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> resultList, ParseException e) {
                        if (e == null) {

                            for (int i = 0; i < resultList.size(); i++) {
                                ParseObject parseObject = resultList.get(i);
                                String loyola_time = parseObject.getString("Loyola");
                                String sgw_time = parseObject.getString("SGW");

                                Home.loyola_schedule.add(loyola_time);
                                Home.sgw_schedule.add(sgw_time);
                            }

                            setupMainActivity();

                        } else {
                            // fail to retrieve parse data :: need to alert data cannot be retrieved
                            Log.e("ParseException", e.toString());
                        }
                    }
                });
            }else{
                // from monday to thursday schedule
                ParseQuery<ParseObject> schedule_query = ParseQuery.getQuery("regular");
                schedule_query.selectKeys(Arrays.asList("Loyola", "SGW"));
                schedule_query.orderByAscending("order");

                schedule_query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> resultList, ParseException e) {
                        if (e == null) {

                            for (int i = 0; i < resultList.size(); i++) {
                                ParseObject parseObject = resultList.get(i);
                                String loyola_time = parseObject.getString("Loyola");
                                String sgw_time = parseObject.getString("SGW");

                                Home.loyola_schedule.add(loyola_time);
                                Home.sgw_schedule.add(sgw_time);
                            }

                            setupMainActivity();

                        } else {
                            // fail to retrieve parse data :: need to alert data cannot be retrieved
                            Log.e("ParseException", e.toString());
                        }
                    }
                });
            }

        }
    }

    // Gurantee the splashscreen shows at least 3000 ms
    public void setupMainActivity(){
        end = System.currentTimeMillis();
        long leftTime = 3000 - (end - start);

        if(leftTime < 0){
            startActivity(new Intent(this, MainActivity.class));
        }else{
            try {
                Thread.sleep(leftTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            startActivity(new Intent(this, MainActivity.class));
        }

        finish();
    }
}
