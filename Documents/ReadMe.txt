Parse data input format:
Should be 13:30 this kind of format.

Assume we only have one time zone for “Every 20 mins a bus”, and we deal with non equal quantities time schedule situation by dealing with “” empty string.

Departures from Loyola	Departures from S.G.W
7:45	7:45
8:15	8:15
8:45	8:45
9:00	9:15
9:15	9:30
9:45	9:45
10:00	10:15
10:15	10:30
11:00	10:45
11:15	11:30
11:45	11:45
12:00	12:15
12:15	12:45
12:45	13:00
13:15	13.15
13:30	13:45
13:45	14:00
14:15	14:15
14:30	14:45
14:45	15:00
15:15	15:15
15:30	15:45
15:45	16:00
16:15	16:15
16:30	16:45
16:45	17:15
17:15	17:45
18:15	18:15
18:45	18:45
19:15	19:15
19:45	19:45